class RegisterPage{

    get firstNameInput() {return $('input[name=firstName]')};
    get lastNameInput() {return  $('input[name=lastName]')};
    get emailInput() {return $('input[name=email]')};
    get passwordInput() {return $('input[type=password]')};
    get createButton() {return $('//button[contains(., "Create")]')};
};

module.exports = RegisterPage;
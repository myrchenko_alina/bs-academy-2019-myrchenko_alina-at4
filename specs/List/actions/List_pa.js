const ListPage = require('../page/List_po');
const page = new ListPage();

class ListActions{
    getListTitle(){
        return page.listTitle.getText();
    }

    getListCreator(){
        return page.listCreator.getText();
    }
};

module.exports = ListActions;
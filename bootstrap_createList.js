const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class Client {
        static async getBearerToken(url, args){
            fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            })
            .then(res => {
                res.json()
            })
            .then(data => {
                return data.data.access_token;
            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            });
        };

        static async getUserId(url, args){
            const token = await this.getBearerToken(url, args);
            fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            .then(res => {
                res.json()
            })
            .then(data => {
                return data.data.id;
            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            });
        };

        static async sendCreateListRequest(url, args) {
            const userId = await this.getUserId(url, args);
            const token = await this.getBearerToken(url, args);
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    user_id: userId,
                    name: args.listName,
                    updated_at: Date.now(),
                    created_at: Date.now(),
                })
            });
        };
    };

    async function createList(url, args) {

        console.log(`Creating list for ${url}`);
        const response = await Client.sendCreateListRequest(url, args);

        if (response.status === 200) {
            console.log(`List is created on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating List=========');
        createList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
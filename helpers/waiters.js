class CustomWaits {
    forSpinner() {
        const spinner = $('div#preloader');
        spinner.waitForDisplayed(3000);
        spinner.waitForDisplayed(10000, true);

    }

    forNotificationToDisappear() {
        const notification = $('div.toast div');
        notification.waitForDisplayed(5000, true);
    }

    forModal(){
        const modal = $('//div[@class = "modal is-active"]');
        modal.waitForExist(1500);
    }

}

module.exports =  new CustomWaits;
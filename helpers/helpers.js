const LoginActions = require('../specs/Login/actions/Login_pa');
const page = new LoginActions();
const credentials = require('./../testData.json');

class HelpClass 
{

    clickItemInList(name) {
        const place = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]`);
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        place[0].scrollIntoView();
        place[0].click();
    }

    browserClick(elm){
        return browser.execute((e) => {
            document.querySelectorAll(e).click();
        }, elm);
    }

    browserClickOnArrayElement(elm, index){
        return browser.execute((e, i) => {
            document.querySelectorAll(e)[i - 1].click();
        }, elm, index);
    }

    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.clickLoginButton();
    }
    
    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(email);
        page.enterPassword(password);
        page.clickLoginButton();
    }

    selectItemFromList() {
       
    }

    getRandomEmail(){
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < 6; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        result += "@mail.com"
        return result;
    }
    

            

}

module.exports = new HelpClass;
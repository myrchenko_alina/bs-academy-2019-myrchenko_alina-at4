const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class Client {
        static async getBearerToken(url, args){
            fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            })
            .then(res => {
                res.json()
            })
            .then(data => {
                return data.data.access_token;
            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            });
        };
        static async getUserId(url, args){
            const token = await this.getBearerToken(url, args);
            fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            .then(res => {
                res.json()
            })
            .then(data => {
                return data.data.id;
            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            });
        };

        static async getUserLatestListID(url, args){
            const userId = await this.getUserId(url, args);
            fetch(`${url}/users/${userId}/lists`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${this.getBearerToken(url, args)}`
                }
            })
            .then(res => {
                res.json()
            })
            .then(data => {
                return data.data[0].id;
            })
            .catch((error) => {
                console.log(JSON.stringify(error));
            });
        }

        static async sendDeleteListRequest(url, args) {
            const token = await this.getBearerToken(url, args);
            const listId = await this.getUserLatestListID(url, args);
            return fetch(`${url}/user-lists/${listId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
        };
    };

    async function deleteList(url, args) {

        console.log(`Deleting list for ${url}`);
        const response = await Client.sendDeleteListRequest(url, args);

        if (response.status === 200) {
            console.log(`List is deleted on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully delete list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Deleting list=========');
        deleteList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
        console.log('========Finish=========');
    });
}
